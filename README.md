# Circa

A script to take circular screenshots.

## How To Use

1. Open a terminal and run `circa`
2. Follow the on-screen instructions
3. Your screenshot will be saved in the current directory

## Installation

### From Source

Dependencies:
- gnome-screenshot
- xdotool
- ImageMagick
- bc

1. Clone the repository with `git clone https://codeberg.org/X-Industries/circa.git`
2. Give the appropriate permissions by running `chmod +x INSTALL`
3. Install by running `sudo ./INSTALL`
4. Delete the `circa` directory

### Arch Linux

1. [Download the package](https://codeberg.org/attachments/a022626c-f4c9-41aa-8ab5-30ac0916c2d0)
2. Install using `sudo pacman -U /path/to/file`

### Debian/Ubuntu

1. [Download the DEB package](https://codeberg.org/attachments/a4c38bbe-17dc-43be-b3f6-3ecfa9529f37)
2. Install using `sudo apt install /path/to/file`
